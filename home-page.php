<?php //Template Name:HOME
get_header();
	if ( is_front_page())
	{
	if ( creative_theme_is_companion_active() ) {
		// Front page content
		get_template_part('sections/home','slider');
		$fc_home = absint(get_theme_mod('fc_home', 1));
		if ( $fc_home == 1) {
	        get_template_part('sections/footer', 'callout');
	    }
		//****** get home callout ********
		if($sections = json_decode(get_theme_mod('home_reorder'),true)) {
			foreach ($sections as $section) {
				$data = "home_".$section;
				$data1 = absint(get_theme_mod($data, 1));
				if ( $data1 == "1") {
					get_template_part('sections/home', $section);
				}
			}
		} else {
			get_template_part('sections/home', 'service');
	        get_template_part('sections/home', 'portfolio');
	        $blog_home = absint(get_theme_mod('blog_home', 1));
			if ( $blog_home == 1) {
				get_template_part('sections/home','blog');
			}
		}
	} else { 
	    get_template_part( 'no', 'content' );
	}	
	get_footer();
	}else if(is_page()){
		get_template_part('page');
	} else {
		get_template_part('index');
	}