<?php
get_header();
get_template_part('header_call_out');
?>
<!-- Main Content -->
<div class="content margin-top60 margin-bottom60">
	<div class="container">
		<div class="row">
			<div class="posts-block col-lg-12 col-md-12 col-xs-12"><?php
				if(have_posts()):
					while(have_posts()): the_post();
						get_template_part('loop');
						creative_page_nav_link();
						
                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;
					endwhile;
				endif;?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>