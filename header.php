<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">				
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<?php   
	    if ( function_exists( 'wp_body_open' ) )
	    wp_body_open();
	    ?>
	    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'creative' ); ?></a>

		<div class="page-mask">
			<div class="page-loader">
				<div class="spinner"></div>
				<?php esc_html_e('Loading...','creative'); ?>
			</div>
		</div>
		<!-- Wrap -->
		<div class="wrap">
			<!-- Header -->
			<header id="header" >
			<!-- Header Top Bar -->
				<div class="top-bar">
					<div class="slidedown collapse">
						<div class="container">
							<?php 
							$header_social_media_in_enabled = absint(get_theme_mod('header_social_media_in_enabled', 1));
							if ($header_social_media_in_enabled == 1) {  ?>
								<div class="pull-left">
									<ul class="social pull-left creative_social_header">
										<?php 
										$fb_link = get_theme_mod('fb_link');
	                                	if (!empty ($fb_link)) { ?>
											<li class="facebook"><a href="<?php echo esc_url(get_theme_mod('fb_link')); ?>"><i class="fab fa-facebook-f"></i></a></li>
										<?php } 
										$twitter_link = get_theme_mod('twitter_link');
	                                	if (!empty ($twitter_link)) { ?>
											<li class="twitter"><a href="<?php echo esc_url(get_theme_mod('twitter_link')); ?>"><i class="fab fa-twitter"></i></a></li>
										<?php } 
										$qq_link = get_theme_mod('qq_link');
	                                	if (!empty ($qq_link)) { ?>
											<li class="dribbble"><a href="<?php echo esc_url(get_theme_mod('qq_link')); ?>"><i class="fab fa-dribbble"></i></a></li>
										<?php } 
										$linkedin_link = get_theme_mod('linkedin_link');
	                                	if (!empty ($linkedin_link)) { ?>
											<li class="linkedin"><a href="<?php echo esc_url(get_theme_mod('linkedin_link')); ?>"><i class="fab fa-linkedin-in"></i></a></li>
										<?php } 
										$vk_link = get_theme_mod('vk_link');
	                                	if (!empty ($vk_link)) { ?>
											<li class="rss"><a href="<?php echo esc_url(get_theme_mod('vk_link')); ?>"><i class="fa fa-rss"></i></a></li>
										<?php }  
										$youtube_link = get_theme_mod('youtube_link');
	                                	if (!empty ($youtube_link)) { ?>										
	                                		<li class="youtube"><a href="<?php echo esc_url(get_theme_mod('youtube_link')); ?>"><i class="fab fa-youtube"></i></a></li>
										<?php } 
										$instagram = get_theme_mod('instagram');
	                                	if (!empty ($instagram)) { ?>										
	                                		<li class="instagram"><a href="<?php echo esc_url(get_theme_mod('instagram')); ?>"><i class="fab fa-instagram"></i></a></li>
										<?php }  ?>
										</ul>
								</div>
							<?php }   
                            $email_id = get_theme_mod('email_id');
                            $phone_no = get_theme_mod('phone_no'); 
                            if (!empty ($email_id) || !empty ($phone_no)) { ?>
								<div class="phone-login pull-right">
									<?php if (!empty ( $phone_no ) ) { ?>
										<a class="creative_social_phone"><i class="fa fa-phone"></i> <?php esc_html_e('Call Us : ','creative'); ?><?php echo esc_html( $phone_no ); ?></a>
									<?php } if (!empty ($email_id)) { ?>
										<a class="creative_social_email"><i class="fa fa-envelope"></i><?php esc_html_e('Email : ','creative'); ?><?php echo esc_html( $email_id ); ?></a>
									<?php } ?>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<!-- /Header Top Bar -->
				<!-- Main Header -->
				<div class="main-header">
					<div class="container">
						
						<!-- TopNav -->
						<div class="topnav navbar-header">
							<a class="navbar-toggle down-button" data-toggle="collapse" data-target=".slidedown">
							<i class="fa fa-angle-down icon-current"></i>
							</a>
						</div>
						<!-- /TopNav-->
						<!-- Logo -->
						<div class="logo pull-left">
								
							<?php
                            if (has_custom_logo()) { ?>
								<?php the_custom_logo();  ?>
							<?php } if (display_header_text() == true) { ?>
								<a href="<?php echo esc_url(home_url( '/' )); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
						 			<h1 class="site-titlee"><?php echo esc_html(get_bloginfo('name'));  ?></h1>
						 		</a>
						 	<?php } ?>
								
							<?php if (display_header_text() == true) { ?>
								<p class="site-desc"><?php echo esc_html(get_bloginfo('description'));  ?></p>
							<?php } ?>
						</div>
						<!-- /Logo -->
						<div id="site-navigation" class="main-navigation" role="navigation">
							<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i class="fa fa-bars"></i></button>
						<!-- Mobile Menu -->
						<div class="mobile navbar-header">
							<a class="navbar-toggle" data-toggle="collapse" href=".navbar-collapse">
							<i class="fa fa-bars fa-2x"></i>
							</a>
						</div>
						<!-- /Mobile Menu -->
						
						<!-- Menu Start -->
						<nav  class="collapse navbar-collapse menu">
							<?php  wp_nav_menu( array(
								'theme_location' => 'primary',
								'menu_class'     => 'nav navbar-nav sf-menu',
								'fallback_cb'    => 'creative_fallback_page_menu',
								'walker'         => new creative_nav_walker(),
							)
							); ?>
						</nav>
						<!-- /Menu -->
					</div>
					</div>
				</div>
				<!-- /Main Header -->
			</header>
			<!-- /Header -->
			<!-- Main Section -->
			<div id="content" class="site-content">
			<section id="main" class="demo-2">