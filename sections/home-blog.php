<!-- Latest Posts -->
<div id="latest-posts" class=" margin-top100">
    <div class="container">
        <?php 
        $blog_title1 = get_theme_mod('blog_title', __('Home Blog Title','creative'));
        if ( ! empty ( $blog_title1 ) ) { ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 text-center">
    			    <h2 class="wow bounceIn creative_blog_title"><?php echo esc_html( $blog_title1 ); ?></h2>
                </div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="masonry1 padding-top40"><?php
				$count_posts = wp_count_posts();
				$published_posts = $count_posts->publish;
                $args = array('post_type' => 'post' ,'post__not_in'  => get_option( 'sticky_posts' ), 'posts_per_page' => 4);
                $post_type_data = new WP_Query($args);
				$i=1;
                if($post_type_data->have_posts()){
                    while($post_type_data->have_posts()): $post_type_data->the_post(); ?>
                        <!-- post item -->
                        <div class="item col-lg-3 col-md-3 col-sm-6 post-item wow fadeInUp" id="row-<?php echo esc_attr($i); ?>">
                            <div class="post-img">
                                <?php $img = array('class' => 'img-responsive img-blog');
                                if (has_post_thumbnail()):
                                    the_post_thumbnail('creative_home_post_thumb', $img);
                                endif; ?>
                               
                            </div>
                            <div class="post-content blog-post-content">
                                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                <?php the_excerpt() ; ?>
                            </div>
                            <div class="meta post-meta">
									<div class="post-date post-meta-content">
										<i title="<?php echo esc_html(get_post_time( get_option('date_format'), true )); ?>" class="fa fa-clock"></i>
									</div>
									
									<div class="post-comment post-meta-content">
										<i class="fa fa-comment-o"></i> <?php comments_popup_link('0', '1', '%'); ?>
									</div>
									<div class="post-link post-meta-content">
                                        <?php $read_more = get_theme_mod( 'read_more', __('Read More','creative') ) ?>
										<a title="Read More" class="post-meta-link" href="<?php the_permalink(); ?>"><?php echo esc_html( $read_more ); ?>
                                        </a>
									</div>
								</div>
                        </div>
                        <?php if($i%4==0){ echo "<div class='clearfix'></div>"; } $i++; endwhile;
					} ?>
                <!-- /post item -->
            </div>
        </div>
    </div>
</div>
