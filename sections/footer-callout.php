<div class="creative_call_out" ?>
		<div class="slogan bottom-pad-small">
			<div class="pattern-overlay">
				<div class="container">
					<div class="row">
						<div class="slogan-content">
							<div class="col-lg-10 col-md-10 wow fadeInLeft">
								<?php 
								$fc_title = get_theme_mod( 'fc_title', __('Lorem Ipsum is simply dummy text of the printing and typesetting industry. ','creative') );
								if ( ! empty ( $fc_title ) ) { ?>
									<h2 class="slogan-title creative_footer_call_title"><?php echo esc_html( $fc_title );?></h2><?php
								} ?>
							</div>
							<div class="col-lg-2 col-md-2 wow fadeInRight">
								<?php  
								$fc_btn_txt = get_theme_mod( 'fc_btn_txt', __('Features','creative') );
								if ( ! empty ( $fc_btn_txt ) ) { ?>
									<div class="get-started">
									<a href='<?php echo esc_url( get_theme_mod( 'fc_btn_link' ) ); ?>'  class="btn-special btn-grey pull-right creative_footer_call_text">
									<?php echo esc_html( $fc_btn_txt ); ?></a>
									</div>
								<?php } ?>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>