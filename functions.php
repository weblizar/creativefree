<?php
/** Theme Name	: Creative **/

	/* Get the plugin */
	if ( ! function_exists( 'creative_theme_is_companion_active' ) ) {
	    function creative_theme_is_companion_active() {
	        require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	        if ( is_plugin_active(  'weblizar-companion/weblizar-companion.php' ) ) {
	            return true;
	        } else {
	            return false;
	        }
	    }
	}

	/* Theme Core Functions and Codes */
	require( get_template_directory() . '/functions/menu/default_menu_walker.php');
	require( get_template_directory() . '/functions/menu/creative_nav_walker.php');
	
	require( get_template_directory() . '/assets/custom-header.php');
	require_once(get_template_directory() . '/functions/class-tgm-plugin-activation.php');
	
	
	/*After Theme Setup*/
	add_action( 'after_setup_theme', 'creative_head_setup' );
	function creative_head_setup()
	{	global $content_width;
		//content width
		if ( ! isset( $content_width ) ) $content_width = 750; //px
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on Theme Palace, use a find and replace
     * to change 'creative' to the name of your theme in all the template files.
     */
    load_theme_textdomain( 'creative' );
		add_theme_support( 'post-thumbnails' ); //supports featured image
		// This theme uses wp_nav_menu() in one location.
		register_nav_menu( 'primary', esc_html__( 'Primary Menu', 'creative' ) );
		add_editor_style();
		// theme support
		$args = array('default-color' => '000000',);
		add_theme_support( 'custom-background', $args);
		
		$args = array(
			'flex-width'    => true,
			'width'         => 1583,
			'flex-height'    => true,
			'height'        => 424,
		);
		add_theme_support( 'custom-header', $args );

		// Logo
	    add_theme_support('custom-logo', array(
	        'width' => 250,
	        'height' => 250,
	        'flex-width' => true,
	        'flex-height' => true,
	    ));
		
		add_theme_support( 'automatic-feed-links');
		add_theme_support( 'woocommerce' ); //add woocommerce support
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
        add_theme_support( 'title-tag');
        add_theme_support( 'customize-selective-refresh-widgets' );
		

		add_image_size('creative_blog_full_thumb',1140,506,true);
		add_image_size('creative_blog_left_thumb',750,333,true);
		add_image_size('creative_home_post_thumb',328,247,true);
	}
	

/*** Excerpt More ***/
add_filter('excerpt_more', 'creative_excerpt_more');
function creative_excerpt_more($more) {
	global $post;
	return '<footer class="post-footer"><a href="'.esc_url(get_permalink($post->ID)).'" class="btn btn-color">'.esc_html__("Read More","creative").'</a></footer>';
}
add_filter( 'the_content_more_link', 'creative_read_more_link' );
/*** Read More ***/
function creative_read_more_link() {
	global $post;
	return '<footer class="post-footer"><a href="'.esc_url(get_permalink($post->ID)).'" class="btn btn-color">'.esc_html__("Read More","creative").'</a></footer>';
}
/*** Page pagination ***/
function creative_page_nav_link(){ ?>
	<ul class="pager">
		<li class="previous"><?php previous_post_link('%link'); ?></li>
		<li class="next"><?php next_post_link('%link'); ?></li>
	</ul><?php                         
}
/****--- Navigation for Author, Category , Tag , Archive ---***/
function creative_navigation() { ?>
	<ul class="pager">
		<?php posts_nav_link(); ?>
	</ul>
<?php }


/*
* creative widget area
*/
add_action( 'widgets_init', 'creative_widgets_init');
function creative_widgets_init() {
/*sidebar*/
register_sidebar( array(
		'name' => esc_html__( 'Sidebar Widget Area', 'creative' ),
		'id' => 'sidebar-primary',
		'description' => esc_html__( 'The primary widget area', 'creative' ),
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="title">',
		'after_title' => '</h3>'
	) );
register_sidebar( array(
		'name' => esc_html__( 'Footer Widget Area', 'creative' ),
		'id' => 'footer-widget-area',
		'description' => esc_html__( 'footer widget area', 'creative' ),
		'before_widget' => '<section class="col-lg-3 col-md-3 col-xs-12 col-sm-3 footer-one wow fadeIn %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="light">',
		'after_title'   => '</h3>'
	) );
}
function creative_scripts()
{
	// Google fonts - witch you want to use - (rest you can just remove)
	wp_enqueue_style('OpenSans', 'https://fonts.googleapis.com/css?family=Rock+Salt|Neucha|Sans+Serif|Indie+Flower|Shadows+Into+Light|Dancing+Script|Kaushan+Script|Tangerine|Pinyon+Script|Great+Vibes|Bad+Script|Calligraffitti|Homemade+Apple|Allura|Megrim|Nothing+You+Could+Do|Fredericka+the+Great|Rochester|Arizonia|Astloch|Bilbo|Cedarville+Cursive|Clicker+Script|Dawning+of+a+New+Day|Ewert|Felipa|Give+You+Glory|Italianno|Jim+Nightshade|Kristi|La+Belle+Aurore|Meddon|Montez|Mr+Bedfort|Over+the+Rainbow|Princess+Sofia|Reenie+Beanie|Ruthie|Sacramento|Seaweed+Script|Stalemate|Trade+Winds|UnifrakturMaguntia|Waiting+for+the+Sunrise|Yesteryear|Zeyada|Warnes|Abril+Fatface|Advent+Pro|Aldrich|Alex+Brush|Amatic+SC|Antic+Slab|Candal');
	// CSS
	wp_enqueue_style('bootstrap', get_template_directory_uri() .'/css/bootstrap.css');
	wp_enqueue_style('bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.css');

	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome-5.11.2/css/all.css');
	wp_enqueue_style('animations', get_template_directory_uri() . '/css/animations.css');
	wp_enqueue_style('creative-menu', get_template_directory_uri() . '/css/menu.css');
	wp_enqueue_style('slider-style', get_template_directory_uri() . '/css/slider/slider-style.css');
	wp_enqueue_style('creative-custom', get_template_directory_uri() . '/css/slider/custom.css');
	wp_enqueue_style('prettyPhoto', get_template_directory_uri() . '/css/prettyPhoto.css');
	wp_enqueue_style('creative-mainStyle', get_stylesheet_uri() );
	wp_enqueue_style('creative-green', get_template_directory_uri() . '/css/green.css');
	wp_enqueue_style('creative-theme-responsive', get_template_directory_uri() . '/css/theme-responsive.css');

	// JS
	wp_enqueue_script('navigation', get_template_directory_uri() .'/js/navigation.js', array('jquery'), true, true );
	wp_enqueue_script('bootstrap', get_template_directory_uri() .'/js/bootstrap.js', array('jquery'), true, true );
	wp_enqueue_script('jquery.wait', get_template_directory_uri() .'/js/jquery.wait.js', array('jquery'), true, true );
	wp_enqueue_script('jquery.prettyPhoto', get_template_directory_uri() .'/js/jquery.prettyPhoto.js', array('jquery'), true, true );
	wp_enqueue_script('imagesloaded.pkg', get_template_directory_uri() .'/js/imagesloaded.pkgd.js', array('jquery'), true, true );
	wp_enqueue_script('wow', get_template_directory_uri() .'/js/wow.js', array('jquery'), true, true );
	wp_enqueue_script('masonary', get_template_directory_uri() .'/js/masonry.pkgd.js', array('jquery'), true, true );
	wp_enqueue_script('creative-custom-js', get_template_directory_uri() .'/js/custom.js', array('jquery'), true, true );
	
	if ( is_singular() ) wp_enqueue_script( "comment-reply" );

}
add_action('wp_enqueue_scripts', 'creative_scripts');
?>
<?php
/*** Comment Function ***/ 
function creative_comments($comments, $args, $depth){
	
	extract($args, EXTR_SKIP);
	if ( 'div' == $args['style'] ){
	$tag = 'div';
	$add_below = 'comment';
	} else{
	$tag = 'li';
	$add_below = 'div-comment';
	} ?>
	<<?php echo esc_attr($tag); ?>>
		<div <?php comment_class( empty( $args['has_children'] ) ? 'child' : 'parent' ,'comment') ?> id="<?php esc_attr(comment_ID()) ?>" >
			<div class="">
				<?php if ( $args['avatar_size'] != 0 ) echo wp_kses_post(get_avatar( $comments, 80 ));?>
			</div>
			<div class="comment-des">
				<div class="arrow-comment">
					<div class="comment-by">
						<strong><?php printf(  '%s', get_comment_author_link() ); ?></strong><span class="date"><?php /* translators: %s: comment. */ printf( esc_html__('%1$s at %2$s','creative'), esc_html(get_comment_date()),  esc_html(get_comment_time()) ); ?><?php edit_comment_link( __( '(Edit)','creative' ), '  ', '' );?></span><?php
						if ( $comments->comment_approved == '0' ) : ?>
							<em class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.' ,'creative'); ?></em><br />
							</div><?php
						else: ?>
					<span class="reply"><?php esc_url(comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) )); ?></span>
				</div><?php
				esc_html(comment_text()); ?>
			</div>
			<div class="clearfix"></div><?php
			endif; ?>
		</div><?php
}

/*** Custom Excerpt ***/
function custom_excerpt_length( $length ) {
	return 49;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
?>
<?php
/* Breadcrumbs  */
	function creative_breadcrumbs() {
    $delimiter = '&#173; &#62; &#173;';
	$pre_text = esc_html__('You are Now on: ', 'creative');
    $home = esc_html__('Home','creative'); // text for the 'Home' link
  
    echo '<div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
					<div class="breadcrumbs pull-right">
						<ul>';
    global $post;
    $homeLink = esc_url(home_url());
    echo '<li>' . esc_html($pre_text) . '<a href="' . esc_url($homeLink) . '">' . esc_html($home) . '</a></li>' . esc_html($delimiter) . ' ';
    if (is_category()) {
        global $wp_query;
        $cat_obj = $wp_query->get_queried_object();
        $thisCat = $cat_obj->term_id;
        $thisCat = get_category($thisCat);
        $parentCat = get_category($thisCat->parent);
        if ($thisCat->parent != 0)
            echo wp_kses_post(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
        echo '<li>' . 'Archive by category "' . single_cat_title('', false) . '"' . '</li>';
    } elseif (is_day()) {
        echo '<li><a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_html(get_the_time('Y')) . '</a></li> ' . esc_html($delimiter) . ' ';
        echo '<li><a href="' . esc_url(get_month_link(get_the_time('Y'), get_the_time('m'))) . '">' . esc_html(get_the_time('F')) . '</a></li> ' . esc_html($delimiter) . ' ';
        echo '<li>' . esc_html(get_the_time('d')) . '</li>';
    } elseif (is_month()) {
        echo '<li><a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_html(get_the_time('Y') ). '</a></li> ' . esc_html($delimiter) . ' ';
        echo '<li>' . esc_html(get_the_time('F')) . '</li>';
    } elseif (is_year()) {
        echo '<li>' . esc_html(get_the_time('Y')) . '</li>';
    } elseif (is_single() && !is_attachment()) {
        if (get_post_type() != 'post') {
            $post_type = get_post_type_object(get_post_type());
            $slug = $post_type->rewrite;
            echo '<li>' . esc_html($pre_text) . '<a href="' . esc_url($homeLink) . '/' . esc_url($slug['slug']) . '/">' . esc_html($post_type->rewrite['slug']) . '</a></li> ' . esc_html($delimiter) . ' ';
            echo '<li>' . esc_html(get_the_title()) . '</li>';
        } else {
            $cat = get_the_category();
            $cat = $cat[0];
            echo '<li>' . wp_kses_post(get_the_title()) . '</li>';
        }
    } elseif (!is_single() && !is_page() && get_post_type() != 'post') {
        $post_type = get_post_type_object(get_post_type());
        echo '<li>' . esc_html($post_type->labels->singular_name) .'<li>';
    } elseif (is_attachment()) {
        $parent = get_post($post->post_parent);
        $cat = get_the_category($parent->ID);
        echo '<li><a href="' . esc_url(get_permalink($parent)) . '">' . esc_html($parent->post_title) . '</a></li> ' . esc_html($delimiter) . ' ';
        echo '<li>' . esc_html(get_the_title()) . '</li>';
    } elseif (is_page() && !$post->post_parent) {
        echo '<li>' . esc_html(get_the_title()) . '</li>';
    } elseif (is_page() && $post->post_parent) {
        $parent_id = $post->post_parent;
        $breadcrumbs = array();
        while ($parent_id) {
            $page = get_page($parent_id);
            $breadcrumbs[] = '<li><a href="' . esc_url(get_permalink($page->ID)) . '">' . esc_html(get_the_title($page->ID)) . '</a></li>';
            $parent_id = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        foreach ($breadcrumbs as $crumb)
            echo wp_kses_post($crumb) . ' ' . esc_html($delimiter) . ' ';
        echo '<li>' . esc_html(get_the_title() ). '</li>';
    } elseif (is_search()) {
        echo '<li>' . 'Search results for "' . get_search_query() . '"' . '</li>';
    } elseif (is_tag()) {
        echo '<li>' . 'Posts tagged "' . single_tag_title('', false) . '"' . '</li>';
    } elseif (is_author()) {
        global $author;
        $userdata = get_userdata($author);
        echo '<li>' . 'Articles posted by ' . esc_html($userdata->display_name) . '</li>';
    } elseif (is_404()) {
        echo '<li>' . esc_html_e("Error 404","creative") . '</li>';
    }
    if (get_query_var('paged')) {
        if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author())
            echo ' (';
        if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author())
            echo ')';
    }
    echo '</div></div></ul>';
	}
	
	add_action('tgmpa_register','creative_plugin_recommend');
function creative_plugin_recommend(){
	$plugins = array(
		
        array(
            'name' => __('Weblizar Companion','creative'),
            'slug' => 'weblizar-companion',
            'required' => false,
        ),
		
	);
    tgmpa( $plugins );
}
?>