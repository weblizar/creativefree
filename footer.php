<?php /**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package creative
 */ ?>

		</section>
			<footer id="footer">
				<div class="pattern-overlay">
					<?php if ( is_active_sidebar( 'footer-widget-area' ) ) { ?>
						<!-- Footer Top -->
						<div class="footer-top">
							<div class="container">
								<div class="row">
									<?php  dynamic_sidebar( 'footer-widget-area' );  ?>
								</div>
							</div>
						</div>
						<!-- /Footer Top -->
					<?php } ?>
					<!-- Footer Bottom -->
					
					<div class="footer-bottom">
						<div class="container">
							<div class="row">
								<?php if ( creative_theme_is_companion_active() ) { ?>
									<div class="col-lg-6 col-md-6 col-xs-12">
										<p class="credits creative_footer_customizations">
										<?php 
										$creative_footer_customization = get_theme_mod( 'creative_footer_customization', __('&copy; Copyright 2020. All Rights Reserved','creative') );
										echo esc_html( $creative_footer_customization ); ?>
										
										<a target="_blank" rel="nofollow" href="<?php echo esc_url( get_theme_mod( 'creative_deve_link' ) ); ?>">
											<?php 
											$creative_develop_by = get_theme_mod( 'creative_develop_by' );
											echo esc_html( $creative_develop_by ); ?></a>
										</p>
									</div>
								<?php 
						        }else{ ?>
							        <div class="col-lg-6 col-md-6 col-xs-12">
							            <p class="credits creative_footer_customizations">
							                <?php esc_html_e('&copy; Copyright 2020. All Rights Reserved','creative'); ?>
							            </p>  
							        </div>
								<?php }
								$footer_section_social_media_enbled = absint(get_theme_mod('footer_section_social_media_enbled', 1));
                				if ($footer_section_social_media_enbled == 1) { ?>
									<div class="col-lg-6 col-md-6 col-xs-12">
										<ul class="social pull-right creative_social_footer">
											<?php
				                            $fb_link = get_theme_mod('fb_link');
				                            if (!empty ($fb_link)) { ?>
												<li class="facebook"><a href="<?php echo esc_url(get_theme_mod('fb_link')); ?>"><i class="fab fa-facebook-f"></i></a></li>
											<?php }
                            				$twitter_link = get_theme_mod('twitter_link');
                            				if (!empty ($twitter_link)) { ?>			
												<li class="twitter"><a href="<?php echo esc_url(get_theme_mod('twitter_link')); ?>"><i class="fab fa-twitter"></i></a></li>
											<?php }
				                            $qq_link = get_theme_mod('qq_link');
				                            if (!empty ($qq_link)) { ?>
												<li class="dribbble"><a href="<?php echo esc_url(get_theme_mod('qq_link')); ?>"><i class="fab fa-dribbble"></i></a></li>
											<?php }
				                            $linkedin_link = get_theme_mod('linkedin_link');
				                            if (!empty ($linkedin_link)) { ?>
												<li class="linkedin"><a href="<?php echo esc_url(get_theme_mod('linkedin_link')); ?>"><i class="fab fa-linkedin-in"></i></a></li>
											 <?php }
				                            $vk_link = get_theme_mod('vk_link');
				                            if (!empty ($vk_link)) { ?>
												<li class="rss"><a href="<?php echo esc_url(get_theme_mod('vk_link')); ?>"><i class="fa fa-rss"></i></a></li>
											<?php }
				                            $youtube_link = get_theme_mod('youtube_link');
				                            if (!empty ($youtube_link)) { ?>
												<li class="youtube"><a href="<?php echo esc_url(get_theme_mod('youtube_link')); ?>"><i class="fab fa-youtube"></i></a></li>
											<?php }
				                            $instagram = get_theme_mod('instagram');
				                            if (!empty ($instagram)) { ?>
												<li class="instagram"><a href="<?php echo esc_url(get_theme_mod('instagram')); ?>"><i class="fab fa-instagram"></i></a></li>
											<?php } ?>
										</ul>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<!-- /Footer Bottom -->
				</div>
			</footer>
			
			<?php if ( creative_theme_is_companion_active() ) { 
		    $creative_return_top = absint(get_theme_mod( 'creative_return_top', '1' ));
		    if ( $creative_return_top == "1" ) { ?>
			<!-- Scroll To Top -->
				<a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>
			<?php } } ?>
		</div></div>
		<!-- /Wrap -->
		<?php wp_footer(); ?>
	</body>
</html>